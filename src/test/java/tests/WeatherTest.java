package tests;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import Services.WeatherServices;
import dataprovider.DataproviderClass;
import io.restassured.response.Response;
import util.APIException;
import util.Common;
import util.GenerateReport;
import util.ListenersAPITest;

@Listeners(ListenersAPITest.class)
public class WeatherTest {
	private Response response;
	private static ExtentTest test;

	private ExtentReports report;
	
	@Test(dataProvider = "SearchProvider", dataProviderClass = DataproviderClass.class)
	public void locationWeatherTest(String criteria, String actual) throws APIException {
		try {
			String location = criteria;
			String key = Common.getAPI();
			
			Assert.assertNotEquals(key, "");
			response = WeatherServices.responseValidation(Common.BaseURL + key + "=" + location);
			GenerateReport.getInstance().recordInfo("Requesting Temperature data of "+ location);

			//Validate the status of the resonse
			int code = response.getStatusCode();
			Assert.assertEquals(code, 200);
			GenerateReport.getInstance().recordPass("Successfully connected to Weather Stack API");

			//Validate the response data
			String responseValue = response.jsonPath().getString("location.name");
			Assert.assertEquals(responseValue, actual);
			GenerateReport.getInstance().recordPass("Response is matching the requested data");
			
			GenerateReport.getInstance().recordInfo("Printing Country Data : " + response.jsonPath().getString("location.country"));
			GenerateReport.getInstance().recordInfo("Printing Country City : " + response.jsonPath().getString("location.name"));
			GenerateReport.getInstance().recordInfo("Printing Temperature Data : " + response.jsonPath().getString("current.temperature"));
		} catch (Exception e) {
			throw new APIException("Exception : ", e.toString());
		}
	}
	
	@BeforeTest
	public void setUP() {
		GenerateReport.getInstance().generate();
	}
	
	@AfterTest
	public void finish() {
		GenerateReport.getInstance().recordInfo("Test Finished");
		GenerateReport.getInstance().closeReport();
	}
}
