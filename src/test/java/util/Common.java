package util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

import util.GenerateReport;

public class Common {
	public static String BaseURL = "http://api.weatherstack.com/current?access_key=";

	// Get Weather API key
	public static String getAPI() {
		try {
			FileInputStream propfile = new FileInputStream(".\\application.properties");
			Properties properties = new Properties();
			properties.load(propfile);
			String key = properties.getProperty("ApiKey");

			return key;
		} catch (Exception e) {
			e.printStackTrace();
			Reporter.log("Key failure");
			return null;
		}
	}

	// Take screen shot on failures
	/*
	 * public static void takeSnapShot(WebDriver webdriver, String fileWithPath,
	 * GenerateReport report) { try { TakesScreenshot scrShot = ((TakesScreenshot)
	 * webdriver); File SrcFile = scrShot.getScreenshotAs(OutputType.FILE); File
	 * DestFile = new File(fileWithPath); FileUtils.copyFile(SrcFile, DestFile); }
	 * catch (IOException e) { report.recordFailure("Screen capturing exception"); }
	 * }
	 */
}