package util;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class GenerateReport {
	private static ExtentTest test;
	private static ExtentReports report;
	private static GenerateReport INSTANCE;
	
	private GenerateReport() {        
    }
    
    public static GenerateReport getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new GenerateReport();
        }
        
        return INSTANCE;
    }
	
    public void generate() {
    	report = new ExtentReports(".\\Reports\\APIReport.html");
		test = report.startTest(" Weather API Execution Results");
    }
	
	public void recordInfo(String msg) {
		test.log(LogStatus.INFO, msg);
	}

	public void recordWarning(String msg) {
		test.log(LogStatus.WARNING, msg);
	}
	
	public void recordPass(String msg) {
		test.log(LogStatus.PASS, msg);
	}

	public void recordFailure(String msg) {
		test.log(LogStatus.FAIL, msg);
	}

	public void closeReport() {
		report.endTest(test);
		report.flush();
	}
}
