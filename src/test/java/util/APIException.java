package util;

import org.openqa.selenium.WebDriver;

public class APIException extends Exception { 
	 public APIException(String screen , String errorMessage) {
	        super(errorMessage);
	        
	        GenerateReport.getInstance().recordInfo(screen);
	        GenerateReport.getInstance().recordFailure(errorMessage);       
	    }
}